# Upgrade
# *  https://neo4j.com/docs/migration-guide/current/upgrade-single-instance/
### up 3.5.X -> 4.08 
## https://neo4j.com/docs/migration-guide/current/prepare/
### helpfull video 
# https://www.youtube.com/embed/GcaJ-aVLzr4?autoplay=1&feature=oembed&wmode=opaque
#
######
# NOTES: minimum 3 letter name for database (no "." in name allowed!!!)
######
# hint inside here: https://community.neo4j.com/t/migrate-data-from-3-5-1-community-to-4-0-0-community/14502/9
#
#
#DOCKER
# * https://hub.docker.com/_/neo4j
# *  how to set ENV variable and do configuration
# ++ https://neo4j.com/docs/operations-manual/current/docker/configuration/


# working direcotry
NEOHOME=/home/olli/neo4j/upgrade

STARTPEER=4
ENDPEER=5

# folder to dump data 
NAME=DB
DBNAME=neo4j

DOCKER_USER=7474
DOCKER_GRP=7474

USER=olli
GRP=users

function main {

    for (( p=$STARTPEER; p<=$ENDPEER; p++ ))
    do
         if [ "$1" == "stop" ]; then 
             # for debug
             stopDocker  $p
         elif [ "$1" == "init" ]; then
             # for debug 
             prepare35 $p
         else
            prepare35 $p
            destroyDocker $p
            createDocker $p "3.5.21"
            destroyDocker $p

            prepare408 $p
            createDocker $p "4.0.8"
            destroyDocker $p

            prepare411 $p 
            createDocker $p "4.1.1"
            destroyDocker $p

            fixUp
         fi
    done

}



function prepare35 {

    # using bash-argument to create a name
    name="${NAME}${1}"
    db="${DBNAME}"

      echo "Preparing ${name} for Upgrade 3.5.21"

      # create some folders
      mkdir -m 0744 -p ${NEOHOME}/${name}/data/databases
      # copy the original databases (3.X) to "data" folder for upgrade to 3.5
      cp -R ${NEOHOME}/org/P${1}/cooccsdatabaselocal ${NEOHOME}/${name}/data/databases/${db}

      # cleaning stuff from original data
      rm -R  ${NEOHOME}/${name}/data/databases/${db}/index
      rm -R  ${NEOHOME}/${name}/data/databases/${db}/logs
      rm -R  ${NEOHOME}/${name}/data/databases/${db}/store_lock

      # change ownership to docker user
      sudo chown -R ${DOCKER_USER}:${DOCKER_GRP} ${NEOHOME}/${name}
}

function prepare408 {
    name="${NAME}${1}"
    clone="data35"
    
    echo "Preparing ${name} for Upgrade 4.0.8"

    # make a backup to data folder to folder data35
    sudo cp -r ${NEOHOME}/${name}/data/ -T ${NEOHOME}/${name}/${clone}
    sudo chown -R ${DOCKER_USER}:${DOCKER_GRP} ${NEOHOME}/${name}
}

function prepare411 {
    name="${NAME}${1}"
    clone="data408"

    echo "Preparing ${name} for Upgrade 4.1"

    # make a backup to folder data408
    sudo cp -r ${NEOHOME}/${name}/data/ -T ${NEOHOME}/${name}/${clone}
    sudo chown -R ${DOCKER_USER}:${DOCKER_GRP} ${NEOHOME}/${name}
}
function createDocker {
  
    name="${NAME}${1}"
    version="${2}"
    num="${1}"
    db="${DBNAME}"
    http_port=7474
    bolt_port=7687

    echo "Upgrading database #${1} to version ${2}"

    # a DOCKER neo4j database starting up 
    docker run --name ${name} -p${http_port}:7474 -p${bolt_port}:7687 \
    -d -v ${NEOHOME}/${name}/data:/data -v ${NEOHOME}/${name}/logs:/logs \
    --env NEO4J_AUTH=neo4j/superpass  \
    --env NEO4J_dbms_allow__upgrade="true" \
    --env  NEO4J_dbms_active__database="${db}" \
    neo4j:${version}

    printf "Upgrading Database:${db} "

    # Waits until the neo4j API is available => upgrade finished 
    ### breaks the loop, when "curl" returns "success" 
    until $(curl --output /dev/null --silent --head --fail http://localhost:${http_port}); do
      printf '.'
      sleep 2 
    done
    # additional 5secs
     sleep 5

    echo "Upgrade should be finished now"
    # echo "Database ${name} available: http:${http_port} / bolt:${bolt_port}"



}


function destroyDocker {
    name="${NAME}${1}"
    if [ "$(docker ps -a | grep ${name})" ]; then
       echo "Destroying Docker Container ${name}"
       docker stop ${name} &> /dev/null 
       docker rm ${name} &> /dev/null
    fi
}

function stopDocker {
    name="${NAME}${1}"
    if [ "$(docker ps -a | grep ${name})" ]; then
       echo "Stopping Docker #${1}"
       docker stop ${name} &> /dev/null
    fi
}

function fixUp {
      sudo chown -R ${USER}:${GRP} ${NEOHOME}
}

# call main function with argument
main $1

