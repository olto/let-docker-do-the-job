# NEO4J Upgrade - let docker do the job
Anbei ein kleines Bash-Script, dass die Neo4J Daten auf die neueste Version aktualisiert.
Dazu wird "docker" benötigt, die orginalen Rohdaten und eine Linux Shell - ich versuche aber den Weg ohne Shell zu erklären. (Anleitung zum Nachbauen)

## Wichtig !!
* Upgrades müssen folgendermaßen stattfinden 3.5.21 dann 4.0.8 dann 4.1.1....
* Dritte Datenbank (P3) muss zuerst auf Version 3.4.X gehoben werden, danach funktionert der "normale" Upgradepfad
* ab Version 4 müssen Datenbanken 3-63 Zeichen haben (Punkt ist nicht erlaubt) [zu den Regeln](https://neo4j.com/docs/operations-manual/current/manage-databases/configuration/)

## Ablauf mit nur einer Datenbank (ohne Script)
Docker hat alle Versionen von Neo4j als fertige Dockerfiles auf Dockerhub [https://hub.docker.com/_/neo4j]

Als erstes benötigt man das Upgrade auf Version 3.5.21.

Ich habe dazu die orginalen Daten z.B. aus `P1/cooccsdatabaselocal/*` in ein Verzeichnis `/home/olli/peer1/data` kopiert und mittels Docker-Volume eingebunden (`-v /home/olli/peer1/data/:/data`). (Docker-Volume benutzt den lokalen Ordner im Docker-Container - d.h. Änderungen an den Daten finden im lokalen Ordner statt)

Eventuell unnötig, aber besser fürs Debugging, ist auch ein Verzeicnis für das Logging von neo4j (`-v /home/olli/peer1/logs:/logs`) einzubinden.

Setzen der Zugangsdaten ist mittels EnvironmentVariable und auch das wichtige "allow_upgrade" . Die Schreibweise mit `__` wird [hier](https://neo4j.com/docs/operations-manual/current/docker/configuration/) erlärt.


Befehl für docker:

```
    docker run --name peer1 -p7474:7474 -p7687:7687 \
    -d -v /home/olto/peer1/data:/data -v /home/olto/peer1/logs:/logs \
    --env NEO4J_AUTH=neo4j/superpass  \
    --env NEO4J_dbms_allow__upgrade="true" \
    --env  NEO4J_dbms_active__database="neo4j" \
    neo4j:3.5.21
```

Nun sollte das Docker Image gebaut werden ...und starten....  wenn alles fertig ist unter http://localhost:7474 die Neo4j-web-oberfläche glänzen. (Sonst mal `docker ps -a` aufrufen)
Finden sich die Daten ("SINGLE_NODE", .. ) darin wieder, dann hat es geklappt. Sonst das `debug.log` überprüfen.

### Weiter gehts
Docker Container stoppen `docker stop peer1` und löschen `docker rm peer1`. Nun den obigen Dockerbefehl nur an der Versions-Stelle ändern (eventuell die vorher Datenbanken sichern) und ab zur nächsten Runde (und die nächste dann mit `neo4j:4.1.1`)
```
  docker run ....
   ....
   neo4j:4.0.8
```

# Vorbereitung mit Script 
Das alles geht von Hand, aber ein Script kann dies automatisieren. 

Ordnerstruktur 
```
home/olli/neoUp/.
                /org/P1
                /org/P2 
                  ...
                /org/P5
                neo4j_upgrade_docker.sh          
```         
Der Ordner `/home/olli/neoUp` dient als working directory und wird in Variable `$NEOHOME` gesetzt. Unterhalb des Home-Ordners befindet sich die Rohdaten (siehe Abbildung). Nun  

to-be-continued 
