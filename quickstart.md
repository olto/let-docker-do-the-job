
# Allgemein
der Zugriff auf die Datenbanken erfolgt mittels Neo4j-Client, dem Webbrowser (http://localhost:7474) nach Start des Neo4j Server, einer Schnittstelle genannt Bolt (bolt://localhost:7687) und/oder mittels Java-Embedded Driver.

Ich nutze den Java-Embedded Driver fürs Programmieren und zum Anschauen der Daten dann nen Docker Container, der wiederum den Server startet und ich mit dem Browser den Graphen anschauen kann.

Zum Interagieren mit dem Graphen, z.b. Suche eines Knotens ... hat Neo4j eine DSL namens "Cypher" Bsp `"MATCH (n:SINGLE_NODE {name:"thailand") RETURN n"` findest Du den Knoten mit Namen "thailand".

Cypher ist sehr mächtig und Du benötigst es im weiteren Verlauf ständig. Besonders wichtig ist, dass Du dann von Python aus "cypher" Befehle an die Datenbank schicken kannst und das Ergebnis wieder aus der Datenbank beziehst. (siehe Abschnitt # Transaction)


# Datensatz
Du weisst ja schon, die sind veraltet (Stand Version 3.5.XY) . Also irgendwie upgraden. Gisela exportiert - importiert, ich nutze Docker, da die Datenbanken automatisch upgraden, wenn Du eine neue Version von neo4j-server auf sie jagst.

Natürlich ist der Neo4j-Server/Client,... nicht versionskompatibel !! Also kein Zugriff auf Datensatz Version 3.5.X von einem Client Version 4.0.8 !! (Der Hinweis, nur falls Du dich wunderst - hat mich einen Vormittag gekostet :) )

Generell würden Gisela+ich einen Neo4j Version 4.X (aktuell 4.1.1) empfehlen, war bei uns beiden performanter. 


# Neo4j Datenbank

Egal welche Version, die Datenbank hat einen feste Ordnerstruktur. Die Ordner umfassen `/data, /logs, /conf, /plugins` - die Rohdaten (P1-P5) liegen nicht in diesem Format vor ?! Wäre auch zu einfach.

der Neo4j-Server legt aber die Ordner beim Start an ( beim Upgrade baut er wild den Ordner `/data/*` um) .

Also einfach nen leeren Neo4j-Server starten, den Server die Ordner anlegen lassen. Die "Default" Datenbank heißt dabei "neo4j" und ist mit der "movie"-Datenbank gefüllt.

Server wieder aus, in den Ordner `/data/databases/neo4j` navigieren, alles löschen und mit einem versions-kompatiblen Daten ersetzen. Der Server sollte nach Neustart die neuen Daten anzeigen. Falls Du nen anderen Name als "neo4j" nutzen möchtest - hier stehen die Regel für die Benamung:  https://neo4j.com/docs/operations-manual/current/manage-databases/configuration/ (Missachtung hat mich fast  nen Tag gekostet)

# Neo4j Plugins

kurz zu den Ordner `/plugins` und `/conf`
Es gibt für neo4j plugins. z.B. apoc und gds(graph data-science) - eine Erweiterung mit vielen Algorithmen ...

Dazu müssen versions-kompatible Plugins (*.jar) files im Ordner `/plugin` liegen UND die Plugins in der Konfigurationsdatei `/conf/neo4j.conf` freigeschaltet sein

```
dbms.active_database=neo4j
dbms.security.procedures.unrestricted=gds.*,apoc.*
dbms.security.procedures.whitelist=gds.*,apoc.*
# disable username/passwd
# dbms.security.auth_enabled= false
```

Ach so, ich habe auch ein Username/Passwort gesetzt:  `neo4j/superpass` 
Notwendig ist das nicht, ich schalte es mit `dbms.security.auth_enabled=false` meist ab.


# Programmieren

Wichtig ist, dass Du aus Python raus ne Datenbank öffnen kannst - dann z.B. Cypher Befehle ausführen kannst - die Daten auswerten (aus Transaktion entnehmen) die Datenbank schließen und eine andere Datenbank öffnen kannst. (z.B: Zusammensetzen des globalen Graphen)


Leider kann man mit der Neo4j Community Edition nicht mehrere Datenbanken parallel offen halten (hatte da immer kryptische Errormessages) - daher habe ich das umschifft, in dem ich einen Datenbank öffne- alle Daten entnehme, schließe, nächste öffne. Klappt auch.


Ich nutze für den Zugriff die Version mit dem Java-Embedded Driver, Gisela macht den Zugriff über die Bolt- Schnittstelle.


## Neo4j Transaction
Also die Interaktion mit der neo4j datenbank läuft über Transaktionen. soweit so klar, mir war nicht bewusst, dass die Ergebnisse flüchtig sind, d.h. in Java bekommt man Objekte zurück z.b. Objekte des Typs "Node" wenn man eine Cypher anfrage stellt kann diese aber nicht außerhalb einer Transaktion benutzen.
Der Typ "Node" exisitert nur während der offenen Transaktion. Sobald ich versucht habe das gesamte Node-Objekt außerhalb der Transaktion zu Speichern, hatte ich einen Transaktion-Error.

Hier mal kurz ein bisschen kommentierten Java Code dazu, wie man trotzdem schafft an Daten ranzukommen: 


```
 private  Map<String,String> runCypherQuery(){

        String query = "MATCH (n:SINGLE_NODE) RETURN n LIMIT 25";
        Map<String,String> cypher_result = new HashMap<>();

        //starten einer Transaction
        try ( Transaction tx = Db.getGraphDb().beginTx())
        {
            //Ausführen der cypher query
            Result result = tx.execute(query);
            //Iterieren über die Ergebnisse, wenn es eines gibt
            while (result.hasNext()) {
                //Zwischenspeichern jeder Reihe
                Map<String,Object> row  = result.next();
                
                //Cast des Objekts als Node, das "n" stammt aus der Cypher Query
                Node singleNode = (Node) row.get("n");
                
                //Zugriff auf die Eigenschaften des Nodes und Ablegen in cypher_result
                // hier: Name und Wert von Occur
                cypher_result.put(singleNode.getProperty("name"),singleNode.getProperty("occur"));
            }
        }

        return cypher_result;
    }
```

Es wird eine Transaktion gestartet und danach Ergebnisse ausgewertet. Da man Ergebnisse in Form der Klasse "Node" erwartet, kann man diese Casten und danach z.B: nach den Properties "name" und "occur" abfragen. Leider kann nicht das gesamte Objekt "Node" außerhalb der Transaktion gespeichert werden.

Das ganze kann man nocht weiter Generalisieren, aber denke das ist zu Java spezifisch

to-be-continued.... 

